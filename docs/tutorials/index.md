# NERSC Tutorials

## Modules

- [Lmod Training](https://gitlab.com/NERSC/lmod-training)

## Spack

- [Spack Nightly Build Pipeline](https://software.nersc.gov/ci-resources/spack-nightly-build)
- [Spack CI Pipleline](https://software.nersc.gov/ci-resources/spack-ci-pipelines)

## Containers

- [Shifter for beginners](../development/shifter/shifter-tutorial.md)
