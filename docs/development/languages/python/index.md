# Python

Python is a popular high-level, general-purpose programming language. At NERSC,
Python is used everywhere from system monitoring, data analysis, and visualization to
GPU programming, large scale data processing, and machine learning at scale.

## Python Quickstart

To get started using Python at NERSC, we recommend using the `python` module
provided by NERSC by running the following command:

```console
nersc$ module load python
```

This will initialize a Python environment with many commonly used scientific packages.
Please review the documentation outlined in the following section to customize your
Python environment and productively use Python at NERSC.

## Python Documentation

<!-- * Tutorials -->
* Guides
    * Setting up...
        * [Python at NERSC](nersc-python.md)
        * [Python in Shifter](python-shifter.md)
        * [Python for Perlmutter GPUs](using-python-perlmutter.md)
    * [Using Parallelism in Python](parallel-python.md)
    * [Using Python profiling and debugging tools at NERSC](profiling-debugging-python.md)
    * [Prepping Python code to use GPUs](perlmutter-prep.md)
* Reference
    * [FAQ and Troubleshooting](faq-troubleshooting.md)
* Explanation
    * [Intro to Python at NERSC](python-intro.md)
    * [Comparison of NumPy BLAS backend performance on Perlmutter CPUs](python-amd.md)

## Getting Help with Python

If you have Python questions or problems, please [contact NERSC's online help
desk](https://help.nersc.gov). We also encourage you to take a look at our
[FAQ and troubleshooting page](faq-troubleshooting.md). 

## Helping NERSC with Python

We welcome [user contributions or edits to our
documentation](https://gitlab.com/NERSC/nersc.gitlab.io/-/blob/main/CONTRIBUTING.md).
