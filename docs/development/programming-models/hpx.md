# HPX 

HPX is a C++ Standard Library for Concurrency and Parallelism. It implements all of 
the corresponding facilities as defined by the C++ Standard. Additionally, HPX 
implements functionalities proposed as part of the ongoing C++ standardization 
process. HPX also extends the C++ Standard APIs to the distributed case.

The API exposed by HPX is not only modeled after the interfaces defined by the 
C++11/14/17/20/23 ISO standard, it also adheres to the programming guidelines used by 
the Boost collection of C++ libraries. HPX aims to improve the scalability of 
today's applications and to expose new levels of parallelism which are necessary to 
take advantage of the exascale systems of the future.

## Usage

For the latest information on using HPX please see:

 * https://github.com/STEllAR-GROUP/hpx 
 * [Perlmutter Readiness](../../performance/readiness.md#hpx) page

For how to build HPX and HPX supported applications in general please see:

 * https://hpx-docs.stellar-group.org/latest/html/manual/building_hpx.html

## Build Script for Compiling HPX on Perlmutter

More CMake variables for building HPX can be referred to: https://hpx-docs.stellar-group.org/latest/html/manual/building_hpx.html .

```bash
#!/bin/bash -e

# This is an example script build_hpx.sh 
# To run it: ./build_hpx.sh install Release

module purge 
module load cmake/3.24.3
module load gcc/11.2.0

COMMAND=$1
BUILD_TYPE=$2
OPTION=$3

HPX_PATH=$HOME/src/hpx
BUILD_PATH=$HPX_PATH/build_$BUILD_TYPE
INSTALL_ROOT=$HOME/install
INSTALL_PATH=$INSTALL_ROOT/hpx_$BUILD_TYPE
BOOST_PATH=$HOME/install/boost
JEMALLOC_ROOT=$HOME/install/

mkdir -p $HPX_PATH
mkdir -p $BUILD_PATH
mkdir -p $INSTALL_PATH
cd $HPX_PATH 
git clone https://github.com/STEllAR-GROUP/hpx $HPX_PATH
cd $BUILD_PATH

cmake \
    -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
    -DBOOST_ROOT=$BOOST_PATH \
    -DHPX_WITH_CXX_STANDARD=17 \
    -DHPX_WITH_MALLOC=JEMALLOC \
    -DJEMALLOC_ROOT=${JEMALLOC_ROOT} \
    -DHPX_WITH_CUDA=OFF \
    -DHPX_WITH_FETCH_ASIO=On \
    -DHPX_WITH_APEX=OFF \
    -DHPX_WITH_NETWORKING=OFF \
    -DHPX_WITH_PARCELPORT_MPI=OFF \
    -DHPX_WITH_PARCELPORT_TCP=OFF \
    -DHPX_WITH_LOGGING=OFF \
    -DHPX_WITH_MAX_CPU_COUNT=256 \
    -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH \
    -DHPX_WITH_EXAMPLES=OFF \
    -DHPX_WITH_TOOLS=OFF \
    -Wdev \
    $HPX_PATH

    make -j 8

    if [ "$COMMAND" = "install" ]; then
        make -j 8 install
    fi
fi
```

### CMake Variables Enabling MPI, Cuda support for HPX

- `HPX_WITH_CUDA`: Enable support for CUDA. Use `CMAKE_CUDA_COMPILER` to set the 
CUDA compiler. This is a standard CMake variable, like `CMAKE_CXX_COMPILER`.

- `HPX_WITH_PARCELPORT_MPI`: Enable the MPI parcelport. This enables the use of 
MPI for the networking operations in the HPX runtime. The default value is OFF 
because it’s not available on all systems and/or requires another dependency. 
However, it is the recommended parcelport.

## Building and Running HPX-enabled application on Perlmutter

More details for creating HPX projects can be found here: https://hpx-docs.stellar-group.org/latest/html/manual/creating_hpx_projects.html

Example source code for HPX application 

```cpp
// main.cpp
#include <hpx/hpx_init.hpp>
#include <hpx/future.hpp>
#include <iostream>

int hpx_main(int argc, char *argv[])
{
  auto universal_answer = []() {
    /* do complex computation */
    return 42;
  };

  auto future = hpx::async(universal_answer);

  std::cout << "Answer to the Ultimate Question of " 
  "Life, the Universe, and Everything: " << future.get() << "\n";

  return hpx::finalize();
}

int main(int argc, char* argv[])
{
    return hpx::init(argc, argv);
}
```

Example CMakeLists.txt for building HPX application

```cmake
cmake_minimum_required(VERSION 3.0.0)
project(HPX_APP LANGUAGES CXX)

find_package(HPX REQUIRED)

add_hpx_executable(hpx_hello_world SOURCES main.cpp)
```

To build HPX application:

```console
cd $SRC
mkdir build && cd build
cmake -DHPX_DIR=$HOME/install/hpx_Release/lib64/cmake/HPX/ ..
make
./hpx_hello_world
```

Expected output:

```console
Answer to the Ultimate Question of Life, the Universe, and Everything: 42
```
