# Migrating Apps from Docker Compose

## Overview

If you have an app that was built for a Docker Compose environment, such
as your local workstation or Spin's original Rancher 1 system, you can
usually migrate it to Rancher 2 without a lot of effort.

While the two systems share key concepts, there is no clean, automated
way to _wholly and seamlessly_ convert apps created in a Docker Compose
environment directly for use in Rancher 2.

While migration is not automatic, it _is_ straightforward. Also, in most
cases, no change is needed to container images when migrating.

## High-level Rancher 1 Concepts and Rancher 2 Equivalents

Users familiar with Rancher 1 are accustomed to working with stacks, services,
and containers. These correspond to Rancher 2 as follows:

| Rancher 1     | Rancher 2              |
| ------------- | ---------------------- |
| Environment   | Cluster                |
| n/a           | Project                |
| Stack         | Namespace              |
| Service       | Workload               |
| Container     | Pod                    |
| Load balancer | Ingress                |
| Volume        | Persistent Volume (PV) |

Generally speaking, a _stack_ full of _services_ will equate to a _namespace_
full of _workloads_.

## Replicating Images to the new Registry

A new image registry was introduced with Rancher 2. If you created custom
images, we strongly suggest you start by copying them to the new image registry.

| Rancher 1                                         | Rancher 2                                                                      |
| ------------------------------------------------- | ------------------------------------------------------------------------------ |
| `registry.spin.nersc.gov`                         | `registry.nersc.gov`                                                           |
| _Docker_ registry with a lightweight _Portus_ UI. | _Harbor_ registry with helm chart support, image scanning, and other features. |

Pushing an image to the new registry is simple. For best performance, use a
workstation with a fast network connection.

`docker login` to both registries if you have not already. Then, follow the process
below for each image.

1. Make sure the image is up-to-date locally:

```
docker pull registry.spin.nersc.gov/namespace/image_name:version
```

2. Add a tag:

```
docker tag registry.spin.nersc.gov/namespace/image_name:version registry.nersc.gov/project/image_name:version
```

3. Push the image:

```
docker push registry.nersc.gov/project/image_name:version
```

Note that `registry.nersc.gov` supports hierarchical namespaces, so you can insert additional
"paths" after the project to keep things organized:

```
docker tag registry.spin.nersc.gov/namespace/image_name:version registry.nersc.gov/project/namespace/image_name:version
```

## Mapping Rancher 1 Compose Syntax to Rancher 2 UI Operations

For most users, the simplest way to migrate an app from Rancher 1 to Rancher 2
is to simply work through the `docker-compose.yml` file side-by-side with the
Rancher 2 UI and set up the app correspondingly.

Follow the guide below as you work through the `docker-compose.yml` for your
existing app. Except as noted, the Rancher 2 directions refer to the workload
Deploy or Edit page.

| Rancher 1                | Rancher 2 |
| ------------------------ | --------- |
| `image:`                 | Specify the image under "Docker Image". |
| `environment:`           | Under the "Environment Variables" section, enter names and values as needed. |
| `volumes:`               | Under the "Volumes" section, click "Add Volume" and select "Add a new persistent volume" (equivalent to a Rancher NFS volume) or "Bind-mount a directory from the node" (for Global File System mounts). |
| `secrets:`               | Rancher 2 can populate secrets into variables or files. (Note that the secret will need to be created first; under "Resources", select "Secrets" and click "Add Secret".) <br><br> To populate a secret into a variable, under the "Environment Variables" section, click "Add From Source" and select "Secret", then choose the secret, key, and supply the variable name. <br><br> To populate a secret into a file, under the "Volumes" section, click "Add Volume" and select "Use a secret", and enter the secret details. |
| `user:`                  | Under the "Command" section, enter the UID under "User ID" and the GID under "Filesystem Group". <br><br> (Click "Show advanced options" in the bottom right if the "Command" section is not shown.) |
| `cap_add:` & `cap_drop:` | Under the "Security & Host Config" section (click "Show advanced options" in the bottom right if it is not shown), select the required capabilities under "Add Capabilities" and select "ALL" under Drop Capabilities". <br><br> (Click "Show advanced options" in the bottom right if the "Security & Host Config" section is not shown.) |
