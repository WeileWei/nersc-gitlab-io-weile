# Securing a web site with LetsEncrypt

LetsEncrypt is a popular way to obtain a self-renewing web certificate to
secure a web site. With LetsEncrypt, automated routines handle the tedious
validation, renewal, and installation of updated certificates. Plus, it's
free.

However, the standard LetsEncrypt process assumes that web certificates
are stored and accessed via a conventional file system, whereas in a
Kubernetes-based systems like Rancher, web certificates are stored as
secrets and referenced by ingress controllers.

[This slide deck (PDF)](https://drive.google.com/file/d/1N81QvkE5raeAas4Yx2DwVctK2uYQ_m5_/view)
describes NERSC user [Chris Harris](https://www.kitware.com/chris-harris/)'s
process for implementing LetsEncrypt in a Rancher environment.

[This video (MP4)](https://drive.google.com/file/d/16Iujbfaup4T-FYbYqOTzCNCOwlrx0iQy/view)
shows Chris demonstrating the approach for NERSC staff.
